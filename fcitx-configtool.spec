Name:		fcitx-configtool
Version:	0.4.10
Release:	2
Summary:	Gtk+-based configuring tools for Fcitx
License:	GPL-2.0-or-later
URL:		https://github.com/fcitx/fcitx-configtool
Source0:	http://download.fcitx-im.org/fcitx-configtool/%{name}-%{version}.tar.xz

BuildRequires:	gcc, cmake, fcitx-devel, gettext, intltool, libxml2-devel
BuildRequires:	gtk2-devel, iso-codes-devel, libtool, unique-devel, gtk3-devel, unique3-devel
Requires:	fcitx


%description
fcitx-config-gtk and fcitx-config-gtk3 are Gtk based configuring tools for
Fcitx.


%prep
%setup -q -n %{name}-%{version}

%build
%cmake -DENABLE_GTK3=ON -DENABLE_GTK2=ON
%cmake_build

%install
%cmake_install

%find_lang %{name}

%files -f %{name}.lang
%doc README
%license COPYING
%{_bindir}/*


%changelog
* Wed Nov 20 2024 Funda Wang <fundawang@yeah.net> - 0.4.10-2
- adopt to new cmake macro

* Tue Dec 22 2020 weidong <weidong@uniontech.com> - 0.4.10-1
- Initial Package
